# Face Recognition CAFFEMODEL

## Dependencies
---------------
* imutils         0.5.3
* joblib          0.14.1
* numpy           1.18.1
* opencv-python   4.1.2.30
* scipy           1.4.1
* setuptools      42.0.2

> $ pip install -r requirements.txt

## Run the proyect
------------------

## The model can be run in 2 different ways:

### with a Web Cam of your Pc:
> python recognize.py --detector face_detection_model \ --embedding-model openface_nn4.small2.v1.t7 \
--recognizer output/recognizer.pickle \ --le output/le.pickle

### With a folder of images:
> python recognize.py --detector face_detection_model \ --embedding-model openface_nn4.small2.v1.t7 \ --recognizer output/recognizer.pickle \ --le output/le.pickle --image 1


## Re-train the model
---------------------

### first you need have the pictures:

> if you want re-train the model, you need have in the folder dataset one folder with images. The name of folder must be the name of the person in the pictures.

### second you need run extract_embeddings.py:

> python extract_embeddings.py --dataset dataset --embeddings output/embeddings.pickle \
--detector face_detection_model --embedding-model openface_nn4.small2.v1.t7

### for finish you need run train_model.py:

> python train_model.py --embeddings output/embeddings.pickle \
--recognizer output/recognizer.pickle --le output/le.pickle